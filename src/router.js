const express = require('express');
const router = express();

// router endpoint
router.get("/", (req, res) => res.send("Welcome!!!!"));

// router endpoint to sum 2 numbers
router.get("/add", (req, res) => {
    try{
        const sum = req.query.a + req.query.b;
        res.send(sum.toString());
    } catch(e) {res.sendStatus(500);} 
});


// endpoint 3
router.use(express.urlencoded({extended: true}));
router.use(express.json());

module.exports = router;